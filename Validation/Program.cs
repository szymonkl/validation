﻿using System;
using Validation.Others;
using Validation.Validators;

namespace Validation
{
    class Program
    {
        static void Main(string[] args)
        {
            Studnia studniaOK = new Studnia();
            try
            {
                studniaOK.Nazwa = "Dominiak";
                studniaOK.Depresja = 2.0;
                studniaOK.Miazszosc = 15;
                studniaOK.Wydatek = 1000;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Studnia studniaNotOK = new Studnia();
            try
            {
                studniaOK.Nazwa = "";
                studniaOK.Depresja = -2.0;
                studniaOK.Miazszosc = 1000;
                studniaOK.Wydatek = -100;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
