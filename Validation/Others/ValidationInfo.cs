﻿using System.Collections.Generic;

namespace Validation.Others
{
    public class ValidationInfo
    {
        public string Name { get; set; }
        public List<string> Message { get; set; } = new List<string>();
    }
}