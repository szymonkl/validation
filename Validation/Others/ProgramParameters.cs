﻿using System.Collections.Generic;

namespace Validation.Others
{
    public static class ProgramParameters
    {
        public static Dictionary<string, double> MinValues { get;  set; }
        public static Dictionary<string,double> MaxValues { get; set; }

         static ProgramParameters()
         {
             MinValues = new Dictionary<string, double>
             {
                 {"Wydatek", 0},
                 { "Depresja", 0},
                 { "Miazszosc", 0.1}
             };

             MaxValues = new Dictionary<string, double>
             {
                 {"Wydatek", 10000},
                 { "Depresja", 100},
                 { "Miazszosc", 100}
             };

         }
    }
}
