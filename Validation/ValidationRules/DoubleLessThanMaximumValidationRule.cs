﻿using Validation.Others;

namespace Validation.ValidationRules
{
    public class DoubleLessThanMaximumValidationRule : IValidationRule
    {
        public bool IsValid(object target, ValidationInfo info)
        {
            var value = (double) target;
            bool isValid = value < ProgramParameters.MaxValues[info.Name];
            if (!isValid)
            {
                info.Message.Add(info.Name + " jest za duża!");
            }
            return isValid;
        }
    }
}
