﻿using Validation.Others;

namespace Validation.ValidationRules
{
    public interface IValidationRule
    {
        bool IsValid(object target, ValidationInfo info);
    }
}
