﻿using Validation.Others;

namespace Validation.ValidationRules
{
    public class DoubleGreaterThanMinimumValidationRule : IValidationRule
    {
        public bool IsValid(object target, ValidationInfo info)
        {
            var value = (double) target;
            var isValid =  value > ProgramParameters.MinValues[info.Name];
            if (!isValid)
            {
                info.Message.Add(info.Name + " jest za mała!");
            }
            return isValid;
        }
    }
}
