﻿using System.Collections.Generic;
using Validation.ValidationRules;

namespace Validation.Validators
{
    public class StringValidator : ValidatorBase
    {
        public StringValidator()
        {
            ValidationRules = new List<IValidationRule>
            {
                new StringNotNullOrEmptyValidationRule()
            };
        }
    }
}
