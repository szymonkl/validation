﻿using System.Collections.Generic;
using Validation.ValidationRules;

namespace Validation.Validators
{
    public class DoubleValidator : ValidatorBase
    {
        public DoubleValidator()
        {
            ValidationRules = new List<IValidationRule>
            {
                new DoubleLessThanMaximumValidationRule(),
                new DoubleGreaterThanMinimumValidationRule()
            };
        }
    }
}
