﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Others;
using Validation.Validators;

namespace Validation
{
    public class Studnia
    {
        public string Nazwa
        {
            get { return _nazwa; }
            set
            {
                _validator = new StringValidator();
                var info = new ValidationInfo {Name = nameof(Nazwa)};
                if (_validator.Validate(value, info))
                {
                    _nazwa = value;
                }
                else
                {
                    throw new Exception(info.Message.First());
                }
                
            }
        }

        public double Depresja
        {
            get { return _depresja; }
            set {
                _validator = new DoubleValidator();
                var info = new ValidationInfo { Name = nameof(Depresja) };
                if (_validator.Validate(value, info))
                {
                    _depresja = value;
                }
                else
                {
                    throw new Exception(info.Message.First());
                }
            }
        }

        public double Miazszosc
        {
            get { return _miazszosc; }
            set
            {
                _validator = new DoubleValidator();
                var info = new ValidationInfo { Name = nameof(Miazszosc) };
                if (_validator.Validate(value, info))
                {
                    _miazszosc = value;
                }
                else
                {
                    throw new Exception(info.Message.First());
                }
            }
        }

        public double Wydatek
        {
            get { return _wydatek; }
            set
            {
                _validator = new DoubleValidator();
                var info = new ValidationInfo { Name = nameof(Wydatek) };
                if (_validator.Validate(value, info))
                {
                    _wydatek = value;
                }
                else
                {
                    throw new Exception(info.Message.First());
                }
            }
        }

        private ValidatorBase _validator;
        private string _nazwa;
        private double _depresja;
        private double _miazszosc;
        private double _wydatek;
    }
}
